export enum SortType{
    Asc = "asc",
    Desc = "desc"
}

function isArrayEmptyOrNull(array: any): boolean {
    if(array == null || array.length === 0) {
        return true;
    }
    return false;
} 

export function max(array: number[]): number {
    if(isArrayEmptyOrNull(array)) {
        throw new Error("The array is empty or null!");
    }

    const arraySize: number = size(array);
    let maxNumber: number = Number.NEGATIVE_INFINITY;

    for(let i: number = 0; i < arraySize; i++) {
        let currentNumber: number = array[i];
        if (currentNumber > maxNumber) {
            maxNumber = currentNumber;
        }
    }

    return maxNumber;
}

export function min(array: number[]): number {
    if(isArrayEmptyOrNull(array)) {
        throw new Error("The array is empty or null!");
    }

    const arraySize: number = size(array);
    let minNumber: number = Number.POSITIVE_INFINITY;

    for(let i: number = 0; i < arraySize; i++) {
        let currentNumber: number = array[i];
        if (currentNumber < minNumber) {
            minNumber = currentNumber;
        }
    }

    return minNumber;
}

export function average(array: number[]): number {
    if(isArrayEmptyOrNull(array)) {
        return 0;
    }
    let average: number = 0, sum: number = 0;
    const arraySize = size(array);


    for(let i: number = 0; i < arraySize; i++) {
        const currentNumber: number = array[i];
        sum += currentNumber
    }

    average = sum / arraySize;

    return average;
}

export function std_dev(array: number[]): number {
    if (isArrayEmptyOrNull(array)) return 0;

    const arrayAverage: number = average(array),
            arraySize: number = size(array);

    let sum: number = 0, std_dev: number = 0;

    for(let i: number = 0; i < arraySize; i++) {
        let currentNumber: number = array[i];
        sum += Math.pow(currentNumber-arrayAverage,2);
    }

    std_dev = Math.sqrt(sum/arraySize)

    return std_dev;
}

export function size(array: number[]): number {
    if(isArrayEmptyOrNull(array)) return 0;
    return array.length;
}

export function sort(array: number[], typeSort: SortType): number[] {
    if(isArrayEmptyOrNull(array)) return array;
    let sortedArray: number[] = [...array];
    quickSort(sortedArray, typeSort);
    return sortedArray;
}

function quickSort(array: number[], sortType: SortType): void {
    quickSortHelper(array, 0, size(array)-1, sortType);
}

function quickSortHelper(array: number[], first: number, last: number, sortType: SortType): void {
    if (first < last) {
        let splitPoint: number = (sortType == SortType.Asc ? partitionAsc(array, first, last) : partitionDesc(array, first, last));
        
        quickSortHelper(array, first, splitPoint-1, sortType);
        quickSortHelper(array, splitPoint+1, last, sortType);
    }
}

function partitionAsc(array: number[], first: number, last: number): number {
    let pivotValue: number = array[first],
        leftMark: number = first + 1,
        rightMark: number = last,
        done: boolean = false;

    while (!done) {

        while (leftMark <= rightMark && array[leftMark] <= pivotValue) {
            leftMark += 1;
        }

        while (array[rightMark] >= pivotValue && rightMark >= leftMark) {
            rightMark -= 1;
        }

        if (rightMark < leftMark) done = true;
        else {
            let temp: number = array[leftMark];
            array[leftMark] = array[rightMark];
            array[rightMark] = temp;
        }
    }

    let temp: number = array[first];
    array[first] = array[rightMark];
    array[rightMark] = temp;

    return rightMark;
}

function partitionDesc(array: number[], first: number, last: number): number {
    let pivotValue: number = array[first],
        leftMark: number = first + 1,
        rightMark: number = last,
        done: boolean = false;

    while (!done) {

        while (leftMark <= rightMark && array[leftMark] >= pivotValue) {
            leftMark += 1;
        }

        while (array[rightMark] <= pivotValue && rightMark >= leftMark) {
            rightMark -= 1;
        }

        if (rightMark < leftMark) done = true;
        else {
            let temp: number = array[leftMark];
            array[leftMark] = array[rightMark];
            array[rightMark] = temp;
        }
    }

    let temp: number = array[first];
    array[first] = array[rightMark];
    array[rightMark] = temp;

    return rightMark;

}

export function reverse(array: number[]): number[] {
    if (isArrayEmptyOrNull(array)) return array;
    let arrayReversed: number[] = [];

    for(let i: number = size(array)-1, j = 0; i >= 0; i--, j++) {
        arrayReversed[j] = array[i];
    }

    return arrayReversed;
}