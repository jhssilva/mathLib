import { size, max, min, average, std_dev, sort, SortType, reverse} from "../src/index";

describe("Max function", () => {
  it("Returns the correct number with only negative numbers", () => {
    const array: number[] = [-2,-3,-4];
    expect(max(array)).toEqual(-2);
  });

  it("Returns the correct number with negative and positive numbers", () => {
    const array: number[] = [-2,0,10];
    expect(max(array)).toEqual(10);
  });

    it("Returns the correct number with only positive numbers", () => {
    const array: number[] = [20,0,10];
    expect(max(array)).toEqual(20);
  });

  it("Returns an error when the array is empty", () => {
    const array: number[] = [];
    expect(() => max(array)).toThrow();
  });
});

describe("Min function", () => {
  it("Returns the correct number with only negative numbers", () => {
    const array: number[] = [-2,-3,-4];
    expect(min(array)).toEqual(-4);
  });

  it("Returns the correct number with negative and positive numbers", () => {
    const array: number[] = [-2,0,10];
    expect(min(array)).toEqual(-2);
  });

  it("Returns the correct number with only positive numbers", () => {
    const array: number[] = [20,0,10];
    expect(min(array)).toEqual(0);
  });

  it("Returns an error when the array is empty", () => {
    const array: number[] = [];
    expect(() => min(array)).toThrow();
  });
});

describe("Average function", () => {
  it("Returns the correct value with only negative numbers", () => {
    const array: number[] = [-2,-3,-4];
    expect(average(array)).toEqual(-3);
  });

  it("Returns the correct value with negative and positive numbers", () => {
    const array: number[] = [-2,0,10];
    expect(average(array)).toEqual(8/3);
  });

  it("Returns the correct value with only positive numbers", () => {
    const array: number[] = [20,0,10];
    expect(average(array)).toEqual(10);
  });

  it("Returns 0 when the array is empty", () => {
    const array: number[] = [];
    expect(average(array)).toEqual(0);
  });
});

describe("Std Dev function", () => {
    it("Returns a value", () => {
    const array: number[] = [20,0,10];
    expect(() => std_dev(array)).not.toBeUndefined();
  });

  it("Returns the correct value 1", () => {
    const array: number[] = [1.55,1.70,1.80];
    expect(std_dev(array)).toBeCloseTo(0.1027);
  });

  it("Returns the correct value 2", () => {
    const array: number[] = [2, 4, 4, 4, 5, 5, 7, 9];
    expect(std_dev(array)).toEqual(2);
  });

  it("Returns 0 when the array is empty", () => {
    const array: number[] = [];
    expect(std_dev(array)).toEqual(0);
  });
});

describe("Size function", () => {
    it("Returns a value", () => {
    const array: number[] = [20,0,10];
    expect(() => size(array)).not.toBeUndefined();
  });

  it("Returns type Number", () => {
    const array: number[] = [20,0,10];
    expect(typeof size(array)).toBe("number");
  });

  it("Returns the correct value 1", () => {
    const array: number[] = [1.55,1.70,1.80];
    expect(size(array)).toEqual(3);
  });

  it("Returns the correct value 2", () => {
    const array: number[] = [2, 4, 4, 4, 5, 5, 7, 9];
    expect(size(array)).toEqual(8);
  });

  it("Returns 0 when the array is empty", () => {
    const array: number[] = [];
    expect(size(array)).toEqual(0);
  });
});

describe("Sort function", () => {
    it("Returns a value, even if it's an empty array", () => {
    const array: number[] = [20,0,10];
    expect(() => sort(array, SortType.Asc)).not.toBeUndefined();
  });

    it("Small Array sorted in Asc order with negatives and positive values", () => {
      const array: number[] = [20,0,10,-10],
        arraySorted: number[] = [-10,0,10,20];
    expect(sort(array, SortType.Asc)).toEqual(arraySorted);
  });

    it("Small Array sorted in Asc order with only positive values", () => {
      const array: number[] = [20,0,10],
        arraySorted: number[] = [0,10,20];
    expect(sort(array, SortType.Asc)).toEqual(arraySorted);
  });

  it("Big Array sorted in Asc order with negatives and positive values", () => {
    const array: number[] = [30,-20,-21, 10, -22, 14, 15, 13, -20],
      arraySorted: number[] = [-22,-21,-20,-20, 10, 13, 14, 15, 30];
    expect(sort(array, SortType.Asc)).toEqual(arraySorted);
  });

    it("Small Array sorted in Desc order with negatives and positive values", () => {
      const array: number[] = [20,0,10,-10],
        arraySorted: number[] = [20, 10, 0,-10];
    expect(sort(array, SortType.Desc)).toEqual(arraySorted);
  });

    it("Small Array sorted in Desc order with only positive values", () => {
      const array: number[] = [20,0,10],
        arraySorted: number[] = [20, 10, 0];
    expect(sort(array, SortType.Desc)).toEqual(arraySorted);
  });

  it("Big Array sorted in Desc order with negatives and positive values", () => {
    const array: number[] = [30,-20,-21, 10, -22, 14, 15, 13, -20],
      arraySorted: number[] = [30, 15, 14, 13, 10, -20, -20, -21, -22];
    expect(sort(array, SortType.Desc)).toEqual(arraySorted);
  });

});

describe("Reverse function", () => {
    it("Returns a value, even if it's an empty array", () => {
    const array: number[] = [20,0,10];
    expect(() => reverse(array)).not.toBeUndefined();
  });

    it("Small Array reversed with negatives and positive values", () => {
      const array: number[] = [20,0,10,-10],
        arrayReversed: number[] = [-10,10,0,20];
    expect(reverse(array)).toEqual(arrayReversed);
  });

    it("Small Array reversed with only positive values", () => {
      const array: number[] = [20,0,10],
        arrayReversed: number[] = [10,0,20];
    expect(reverse(array)).toEqual(arrayReversed);
  });

  it("Big Array reversed with negatives and positive values", () => {
    const array: number[] = [30,-20,-21, 10, -22, 14, 15, 13, -20],
      arrayReversed: number[] = [-20,13,15,14, -22, 10, -21, -20, 30];
    expect(reverse(array)).toEqual(arrayReversed);
  });
});