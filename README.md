# MathLib

## Descrição

O presente trabalho MathLib foi realizado para a disciplina de Software Engineer.
O MathLib têm como objectivo fornecer uma livraria simples para arrays.
A linguagem de programação utilizada foi o Typescript. A framework de testes foi utilizado Jest.

## Instalação

É necessário ter o Node.js instalado na maquina, para poder instalar pacotes com o npm.

Os passos seguintes são necessários,

1. Fazer download do Projecto (via git clone, ou simplesmente fazer download do projecto)
2. Abrir o terminal/consola na pasta do projecto (ArraysLibrary)
3. Executar npm install

Em seguida, será instalado todas as dependencias necessárias, sendo elas definidas no ficheiro package.json.

```
    "@types/jest": "^29.4.0",
    "jest": "^29.4.3",
    "ts-jest": "^29.0.5",
    "typescript": "^4.9.5"
```

## Execução dos testes

Para executar os testes apenas é necessário correr, o seguinte comando,

```
npm run test
```

### Exemplo do resultado dos testes

![Imagem do teste](https://gitlab.com/jhssilva/mathLib/-/raw/main/Img/Tests.png)

## Complilação do Projecto

Se for necessário compilar o projecto para executar em javascript ou para export as funções da livraria, é necessário executar o seguinte comando,

```
npm run prepare
```

## Metodos da Livraria

```
export declare enum SortType {
    Asc = "asc",
    Desc = "desc"
}
export declare function max(array: number[]): number;
export declare function min(array: number[]): number;
export declare function average(array: number[]): number;
export declare function std_dev(array: number[]): number;
export declare function size(array: number[]): number;
export declare function sort(array: number[], typeSort: SortType): number[];
export declare function reverse(array: number[]): number[];
```

## Notas

- O Typescript faz as verificações de types (tipo da variavel) em desenvolvimento (se estes são os corretos). A linguagem não permite inserir types diferentes. Esta é a razão porque não foram feitos testes verificando se a váriavel é do tipo correto, pois a verificação já é feita em desenvolvimento. No entanto, a história é diferente para Javascript. Os testes precisam e devem ser mais extensos. O presente projecto é desenvolvido e testado em Typescript prevenindo variaveis diferentes do que a função/varíavel pretende.

  - Por exemplo, Uma função que espera como argumento um type número, no entanto, recebe um valor null.
    - Em Typescript isto não pode acontecer, esta é uma das razões pela qual a linguagem de programação foi escolhida.

## Erros identificados

- Os testes realizados permitiriram-me identificar erros.

  - Por exemplo, quando na realização dos testes para a implementação do (quick) sort, os testes ficavam num loop infinito. Isto quando eram inseridos valores negativos no array. (Durante os testes)
    - Decidi então verificar a lógica da função quick sort, na qual consegui identificar o erro.

## Autor

- Hugo Silva m53080

## Estado do Projecto

Completo.
